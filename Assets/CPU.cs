﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CPU : MonoBehaviour {
	public float maxS = 11f;
	public float jumpForce = 500f;
	public GameObject player;
	private Rigidbody2D rb2d;
	private Animator anim;
    public bool move = true;
    private SpriteRenderer spr;
	private bool dead = false;
	Vector3 fixedVelocity ;
	private bool flipped = false;
	public ParticleSystem hit;
    private Vector3 iniPos;
    public Vector2 playerMotion;
    // Use this for initialization
    void Start () {
		player = GameObject.FindGameObjectWithTag("Player");
		rb2d = GetComponent<Rigidbody2D>();
        move = true;
        anim = GetComponent<Animator>();
		spr = GetComponent<SpriteRenderer>();
        iniPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (!dead) {
			playerMotion = player.transform.position - transform.position;
			playerMotion.x += 20f;
			playerMotion = playerMotion.normalized;
			if (move) {
				anim.SetFloat ("hvelocity", Mathf.Abs (playerMotion.x));
       
				if (!flipped) {
					gameObject.transform.Translate (new Vector2 (playerMotion.x * Time.deltaTime * maxS, 0f));
				} else {
					gameObject.transform.Translate (new Vector2 (-playerMotion.x * Time.deltaTime * maxS, 0f));
				}
			} else {
				anim.SetTrigger ("attack");
				GameManager.instance.PlayerSubLife (0.001f);
				player.GetComponent<PlayerController> ().OnHit ();
			}
		}
	}
	void FixedUpdate(){
		if(!dead){
        if (move) {
            rb2d.velocity = new Vector2(maxS * Time.fixedDeltaTime * 10, rb2d.velocity.y);
        }
		
		if(move)
		{
			if((playerMotion.x < -0.001f && !flipped) || (playerMotion.x > -0.001f && flipped))
			{
				flipped = !flipped;
				this.transform.rotation = Quaternion.Euler(0, flipped ? 180 : 0, 0);
			}
		}
		}
	}
	void OnCollisionEnter2D(Collision2D collision){
		if(collision.gameObject.tag == "Player"){
            Physics2D.IgnoreCollision (collision.collider,GetComponent<Collider2D>(),true);
		}

	}
    public void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        { 
            move = false;
        }
    }
    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            move = true;
        }

    }
    public void OnHit()
    {
		hit.Play();
    }
    public void Death()
    {
        StartCoroutine(DeadWait());
    }
    IEnumerator DeadWait()
    {
        move = false;
		dead = true;
        anim.SetTrigger("dead");
        yield return new WaitForSeconds(2);
        anim.Play("idle");
        //transform.position = iniPos;
    }
}
