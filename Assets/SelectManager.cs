﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelectManager : MonoBehaviour
{
	public static SelectManager instance;
	public int character;
	void Start () {
		instance = this;
	}
	public void Exit()
	{
		Application.Quit();
	}
	public void Menu()
	{
		SceneManager.LoadScene("MainMenu");
	}
	public void Play(int selected)
	{
		character = selected;
		SceneManager.LoadScene("GamePlay");
	}
}