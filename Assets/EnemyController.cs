﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

	public float maxSpeed = 7f;
	public float speed = 7f;
	private Rigidbody2D rb2d;

	// Use this for initialization
	void Start () {
		rb2d = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		rb2d.AddForce(Vector2.right * speed);		
		float limitedSpeed = Mathf.Clamp(rb2d.velocity.x, -maxSpeed, maxSpeed);
		rb2d.velocity = new Vector2(limitedSpeed, rb2d.velocity.y);

		if (rb2d.velocity.x > -0.01f && rb2d.velocity.x < 0.01f){
			rb2d.velocity = new Vector2(speed, rb2d.velocity.y);
		}
			
	}
	void OnCollisionEnter2D(Collision2D coll)
	{
		if(coll.gameObject.tag == "Player"){
			GameManager.instance.PlayerSubLife(0.05f);
			Debug.Log ("test1");
		}
	}
	void OnTriggerEnter2D(Collider2D col){
		if(col.gameObject.tag == "Player"){
			GameManager.instance.PlayerSubLife (0.05f);
			Debug.Log ("test");
		}
		if(col.gameObject.tag == "Attack"){
			GameManager.instance.highscore += 10;
			Destroy(gameObject);
		}
		if(col.gameObject.tag == "right"){
			speed = speed*-1;
			transform.localScale = new Vector3(-6f, 6f, 6f);
		}
		else if(col.gameObject.tag == "left"){
			speed = -speed;
			transform.localScale = new Vector3(6f, 6f, 6f);
		}
	}
}
