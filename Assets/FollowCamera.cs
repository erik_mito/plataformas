﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour {
    public Camera camera;
    private Vector3 tmpPosition;
	// Update is called once per frame
	void Start(){
		camera = Camera.main;
	}
	void FixedUpdate () {
        tmpPosition = this.transform.position;
        tmpPosition.z = camera.transform.position.z;
        camera.transform.position = tmpPosition;
	}
}
