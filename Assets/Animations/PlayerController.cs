﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Variables publicas para poder modifircar desde el inspector
    public float maxS = 11f;
	public AudioSource punch;
	public AudioSource sword;
    public float jumpForce = 500f;
    //Variables privadas
    private Rigidbody2D rb2d = null;
    private float move = 0f;
	private GameObject CPU;
    private Animator anim;
    private bool flipped = false;
    private bool muerto = false;
	private bool floor = false;
	private bool down = false;
	private bool doubleJump = false;
	private int jump = 0;
    private Vector3 iniPos;
	public ParticleSystem hit;
	private bool jumpAction = false;
	Vector3 fixedVelocity ;
    // Use this for initialization
    void Awake()
    {
        // Obtenemos el rigidbody y lo guardamos en la variable rb2d
        // para poder utilizarla más cómodamente
		CPU = GameObject.Find("CPU");
        rb2d = GetComponent<Rigidbody2D>();
		rb2d = GetComponent<Rigidbody2D>();
        // Obtenemos el Animator Controller para poder modificar sus variables
        anim = GetComponent<Animator>();
        iniPos = transform.position;
    }
	void Update () {
		anim.SetFloat("hvelocity", Mathf.Abs(rb2d.velocity.x));
		anim.SetInteger("jump", jump);
		anim.SetBool("agacharse",down);
		if (floor){
			doubleJump = true;
			jump = 0;
		}

		if (Input.GetButtonDown("Jump")){
			if (floor){
				jump = 1;
				jumpAction = true;
				doubleJump = true;
			} else if (doubleJump){
				jump = 1;
				jumpAction = true;
				doubleJump = false;
			}
		}
			
	}

    // Update is called once per frame
    void FixedUpdate()
    {
        //Miramos el input Horizontal
		fixedVelocity = rb2d.velocity;
		fixedVelocity.x *= 0.75f;
		if(muerto)
		{
			return;
		}
		if(jumpAction)
		{
			rb2d.AddForce(Vector2.up * jumpForce);
			floor = false;
			jumpAction = false;
		}
		else if (floor == true) {
			rb2d.velocity = fixedVelocity;
		}
        move = Input.GetAxis("Horizontal");
        rb2d.velocity = new Vector2(move * maxS * Time.fixedDeltaTime * 10, rb2d.velocity.y);
		if (Input.GetKeyDown (KeyCode.X)) {
			anim.SetTrigger ("attack");
			sword.Play ();
			PlayerHit.instance.attack.enabled = true;
		} 
		if (Input.GetKey(KeyCode.DownArrow)) {
			down = true;
		}
		else{
			down=false;
		}
		if (Input.GetKeyDown (KeyCode.Z)) {
			anim.SetTrigger ("slide");
			punch.Play ();
			PlayerHit.instance.attack.enabled = true;
		} 
        //Miramos si nos estamos moviendo.
        // OJO!! Nunca comparar con 0 floats, nunca será 0 perfecto, siempre hay un error de redondeo
        if(rb2d.velocity.x > 0.001f || rb2d.velocity.x < -0.001f)
        {
            if((rb2d.velocity.x < -0.001f && !flipped) || (rb2d.velocity.x > -0.001f && flipped))
            {
                flipped = !flipped;
                this.transform.rotation = Quaternion.Euler(0, flipped ? 180 : 0, 0);
            }
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.gameObject.tag == "muerte")
        {
            muerto = true;
            anim.SetTrigger("dead");
            StartCoroutine(Wait());

        }
		if(coll.gameObject.tag == "enemy")
		{
			Physics2D.IgnoreCollision (coll.collider,GetComponent<Collider2D>(),true);

		}

		if(coll.gameObject.tag == "floor")
		{
			floor = true;
		}

    }
	public void OnHit()
	{
		hit.Play();
	}
    IEnumerator Wait()
    {
        yield return new WaitForSeconds(2);
        ResetMuerte();
    }
    public void ResetMuerte()
    {
        anim.Play("idle");
        muerto = false;
        transform.position = iniPos;
    }


}