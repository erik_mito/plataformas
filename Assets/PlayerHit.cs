﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHit : MonoBehaviour {

    public GameObject Enemy_Obj;
	public static PlayerHit instance;
	public BoxCollider2D attack;
    // Use this for initializat
    void Start()
    {
        Enemy_Obj = GameObject.FindGameObjectWithTag("Enemy");
		attack = GetComponent<BoxCollider2D>();
		attack.enabled=false;
		instance = this;
    }

    // Update is called once per frame
    void Update()
    {
		
    }

    public void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            if (Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.X))
            {
                Invoke("Hit_Active", 0.1f);
                Debug.Log("Hit");
				PlayerHit.instance.attack.enabled = false;

            }
        }
    }
    void Hit_Active()
    {
        GameManager.instance.EnemySubLife();
        Enemy_Obj.GetComponent<CPU>().OnHit();
    }
}
