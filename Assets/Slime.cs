﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slime : MonoBehaviour {
	public bool flipped = false;
	Rigidbody2D slimeRigid;
	public Transform right;
	public Transform left;
	public bool dir=false;
	// Use this for initialization
	void Start () {
		slimeRigid = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (dir) {
			// go closer to the right one
			transform.position = Vector3.MoveTowards(transform.position,
				right.position,
				Time.deltaTime * 10);

			// reached it?
			if (transform.position == right.position)
				dir = !dir; // go to opposite direction next time
		} else {
			// go closer to the left one
			transform.position = Vector3.MoveTowards(transform.position,
				left.position,
				Time.deltaTime * 10);

			// reached it?
			if (transform.position == left.position)
				Debug.Log ("test");
				dir = !dir; // go to opposite direction next time
		}
	}

}
