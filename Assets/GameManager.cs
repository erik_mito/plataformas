﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
	public GameObject[] player;
	public GameObject diamond;
    public Image plaPic;
    public Image cpuPic;
    public Sprite[] spritesPic;
    public static GameManager instance;
	public GameObject Pause;
    public GameObject enemy;
    public Slider enemyLife;
	public Slider playerLife;
	public int highscore;
	public Text highscoreText;
    int enemySelected =0;
    public Vector3 position = new Vector3(-423, 172, 26);
	// Use this for initialization
	void Start () {
		enemy.GetComponent<CPU>().enabled=false;
		diamond.SetActive (false);
		instance = this;
		highscore = 0;
		highscoreText.text = highscore.ToString("D5");
        GameObject.Instantiate(player[SelectManager.instance.character],position,Quaternion.identity);
        plaPic.sprite = spritesPic[SelectManager.instance.character];
        enemySelected = Random.Range(0, 5);
        cpuPic.sprite = spritesPic[enemySelected];
        if (enemySelected== SelectManager.instance.character)
        {
            enemy.GetComponent<SpriteRenderer>().color =Color.red;
        }
    }
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			Pauseg();
		}
        if (enemyLife.value<=0)
        {
			diamond.SetActive (true);
            highscore+=150;
            highscoreText.text = highscore.ToString("D5");
            enemyLife.value = 1;
			enemy.SetActive (false);
            //enemy.GetComponent<CPU>().Death() ;
        }
	}
	public void Pauseg()
	{
		Pause.SetActive(!Pause.activeSelf);
		if(Pause.activeSelf == true)
		{
			Time.timeScale = 0;
		}
		else
		{
			Time.timeScale = 1;
		}

	}

	public void addHighscore(int value)
	{
		highscore += value;
		highscoreText.text = highscore.ToString("D5");
	}
	public void PlayerSubLife(float damage){
		playerLife.value -= damage;
	}

	public void EnemySubLife(){
		enemyLife.value -= .1f;
	}
	public void Finish()
	{
		SceneManager.LoadScene("Congrats");
	}
	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.gameObject.tag == "Player") {
			enemy.GetComponent<CPU>().enabled=true;
			cpuPic.gameObject.SetActive(true);
			enemyLife.gameObject.SetActive(true);
		}
	}
}
